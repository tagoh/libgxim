# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: libgxim\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-01-18 15:14+0900\n"
"PO-Revision-Date: 2010-11-30 05:53-0500\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Zanata 2.0.2\n"

#: ../libgxim/gximattr.c:223
msgid "Attributes enabled"
msgstr ""

#: ../libgxim/gximattr.c:224
msgid "XIM Attributes allowed to use"
msgstr ""

#: ../libgxim/gximcltmpl.c:549
msgid "GDK Atom"
msgstr ""

#: ../libgxim/gximcltmpl.c:550
msgid "GDK Atom for XIM server"
msgstr ""

#: ../libgxim/gximconnection.c:160 ../libgxim/gximcore.c:428
msgid "Signals for Protocol class"
msgstr ""

#: ../libgxim/gximconnection.c:161
msgid "A list of signal connector for Protocol class"
msgstr ""

#: ../libgxim/gximcore.c:392
msgid "GdkDisplay"
msgstr ""

#: ../libgxim/gximcore.c:393
msgid "Gdk display to use"
msgstr ""

#: ../libgxim/gximcore.c:404
msgid "Selection Window"
msgstr ""

#: ../libgxim/gximcore.c:405
msgid "A GdkWindow for selection"
msgstr ""

#: ../libgxim/gximcore.c:416
msgid "GType of Connection class"
msgstr ""

#: ../libgxim/gximcore.c:417
msgid "A GType that deal with XIM connection"
msgstr ""

#: ../libgxim/gximcore.c:429
msgid "A structure of signals for Protocol class"
msgstr ""

#: ../libgxim/gximmessages.c:491
msgid "Master"
msgstr ""

#: ../libgxim/gximmessages.c:492
msgid "A flag to mark a master object."
msgstr ""

#: ../libgxim/gximmessages.c:502
msgid "All filters"
msgstr ""

#: ../libgxim/gximmessages.c:503
msgid "A flag to enable all of the message filters."
msgstr ""

#: ../libgxim/gximprotocol.c:494
#, c-format
msgid "Invalid packets on _XIM_PROTOCOL: format: %d."
msgstr ""

#: ../libgxim/gximsrvtmpl.c:827
msgid "XIM server name"
msgstr ""

#: ../libgxim/gximsrvtmpl.c:828
msgid "XIM server name that is used to identify the server through X property"
msgstr ""

#: ../libgxim/gximsrvtmpl.c:972
#, c-format
msgid "This server instance is already running"
msgstr ""

#: ../libgxim/gximsrvtmpl.c:976
#, c-format
msgid "Another server instance is already running"
msgstr ""

#: ../libgxim/gximsrvtmpl.c:1034
#, c-format
msgid "Unable to acquire the XIM server owner for `%s'"
msgstr ""

#: ../libgxim/gximsrvtmpl.c:1075
#, c-format
msgid "Unable to add a server atom `%s'."
msgstr ""
