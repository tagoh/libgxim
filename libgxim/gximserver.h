/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * gximserver.h
 * Copyright (C) 2008 Akira TAGOH
 * 
 * Authors:
 *   Akira TAGOH  <akira@tagoh.org>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301  USA
 */
#ifndef __G_XIM_SERVER_H__
#define __G_XIM_SERVER_H__

#include <glib.h>
#include <glib-object.h>
#include <libgxim/gximtypes.h>
#include <libgxim/gximsrvtmpl.h>

G_BEGIN_DECLS

#define G_TYPE_XIM_SERVER			(g_xim_server_get_type())
#define G_XIM_SERVER(_o_)			(G_TYPE_CHECK_INSTANCE_CAST ((_o_), G_TYPE_XIM_SERVER, GXimServer))
#define G_XIM_SERVER_CLASS(_c_)			(G_TYPE_CHECK_CLASS_CAST ((_c_), G_TYPE_XIM_SERVER, GXimServerClass))
#define G_IS_XIM_SERVER(_o_)			(G_TYPE_CHECK_INSTANCE_TYPE ((_o_), G_TYPE_XIM_SERVER))
#define G_IS_XIM_SERVER_CLASS(_c_)		(G_TYPE_CHECK_CLASS_TYPE ((_c_), G_TYPE_XIM_SERVER))
#define G_XIM_SERVER_GET_CLASS(_o_)		(G_TYPE_INSTANCE_GET_CLASS ((_o_), G_TYPE_XIM_SERVER, GXimServerClass))

#define G_XIM_SERVER_ERROR			(g_xim_server_get_error_quark())

/**
 * GXimServer:
 * @parent: a #GXimCore.
 *
 * An implementation of XIM server class
 **/
typedef struct _GXimServerClass		GXimServerClass;
typedef struct _GXimServerPrivate	GXimServerPrivate;
typedef enum _GXimServerError		GXimServerError;

struct _GXimServer {
	GXimServerTemplate parent_instance;

	/*< private >*/
	void (* reserved1) (void);
	void (* reserved2) (void);
	void (* reserved3) (void);
	void (* reserved4) (void);
	void (* reserved5) (void);
	void (* reserved6) (void);
	void (* reserved7) (void);
	void (* reserved8) (void);
};

struct _GXimServerClass {
	GXimServerTemplateClass parent_class;

	/*< private >*/

	/* Padding for future expansion */
	void (* _g_xim_reserved1) (void);
	void (* _g_xim_reserved2) (void);
	void (* _g_xim_reserved3) (void);
	void (* _g_xim_reserved4) (void);
	void (* _g_xim_reserved5) (void);
	void (* _g_xim_reserved6) (void);
	void (* _g_xim_reserved7) (void);
	void (* _g_xim_reserved8) (void);
};

enum _GXimServerError {
	G_XIM_SERVER_ERROR_BEGIN = 128,
};


GType       g_xim_server_get_type       (void) G_GNUC_CONST;
GXimServer *g_xim_server_new            (GdkDisplay  *dpy,
                                         const gchar *server_name,
					 const gchar *default_im_attrs,
					 const gchar *default_ic_attrs);
GQuark      g_xim_server_get_error_quark(void);
gboolean    g_xim_server_is_running     (GXimServer  *server,
                                         GError      **error);
gboolean    g_xim_server_take_ownership (GXimServer  *server,
                                         gboolean     force,
                                         GError      **error);


G_END_DECLS

#endif /* __G_XIM_SERVER_H__ */
