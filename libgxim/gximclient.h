/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * gximclient.h
 * Copyright (C) 2008 Akira TAGOH
 * 
 * Authors:
 *   Akira TAGOH  <akira@tagoh.org>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301  USA
 */
#ifndef __G_XIM_CLIENT_H__
#define __G_XIM_CLIENT_H__

#include <glib.h>
#include <glib-object.h>
#include <gdk/gdk.h>
#include <libgxim/gximtypes.h>
#include <libgxim/gximcltmpl.h>

G_BEGIN_DECLS

#define G_TYPE_XIM_CLIENT		(g_xim_client_get_type())
#define G_XIM_CLIENT(_o_)		(G_TYPE_CHECK_INSTANCE_CAST ((_o_), G_TYPE_XIM_CLIENT, GXimClient))
#define G_XIM_CLIENT_CLASS(_c_)		(G_TYPE_CHECK_CLASS_CAST ((_c_), G_TYPE_XIM_CLIENT, GXimClientClass))
#define G_IS_XIM_CLIENT(_o_)		(G_TYPE_CHECK_INSTANCE_TYPE ((_o_), G_TYPE_XIM_CLIENT))
#define G_IS_XIM_CLIENT_CLASS(_c_)	(G_TYPE_CHECK_CLASS_TYPE ((_c_), G_TYPE_XIM_CLIENT))
#define G_XIM_CLIENT_GET_CLASS(_o_)	(G_TYPE_INSTANCE_GET_CLASS ((_o_), G_TYPE_XIM_CLIENT, GXimClientClass))

#define G_XIM_CLIENT_ERROR		(g_xim_client_get_error_quark())

/**
 * GXimClient:
 * @parent: a #GXimCore.
 *
 * An implementation of XIM client class
 **/
typedef struct _GXimClientClass		GXimClientClass;
typedef struct _GXimClientPrivate	GXimClientPrivate;
typedef enum _GXimClientError		GXimClientError;

struct _GXimClient {
	GXimClientTemplate  parent_instance;

	/*< private >*/
	void (* reserved1) (void);
	void (* reserved2) (void);
	void (* reserved3) (void);
	void (* reserved4) (void);
	void (* reserved5) (void);
	void (* reserved6) (void);
	void (* reserved7) (void);
	void (* reserved8) (void);
};

struct _GXimClientClass {
	GXimClientTemplateClass  parent_class;

	/*< private >*/
	void (* reserved1) (void);
	void (* reserved2) (void);
	void (* reserved3) (void);
	void (* reserved4) (void);
	void (* reserved5) (void);
	void (* reserved6) (void);
	void (* reserved7) (void);
	void (* reserved8) (void);
};

enum _GXimClientError {
	G_XIM_CLIENT_ERROR_BEGIN = 128,
	G_XIM_CLIENT_ERROR_NO_PROC,
};


GQuark      g_xim_client_get_error_quark(void);
GType       g_xim_client_get_type       (void) G_GNUC_CONST;
GXimClient *g_xim_client_new            (GdkDisplay  *dpy,
					 const gchar *server_name);
gboolean    g_xim_client_is_initialized (GXimClient  *client);
gboolean    g_xim_client_open_im        (GXimClient  *client);


G_END_DECLS

#endif /* __G_XIM_CLIENT_H__ */
