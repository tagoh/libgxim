/* 
 * libgxim.h
 * Copyright (C) 2008-2011 Akira TAGOH
 * 
 * Authors:
 *   Akira TAGOH  <akira@tagoh.org>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301  USA
 */
#ifndef __LIBGXIM_H__
#define __LIBGXIM_H__

#include <libgxim/gximtypes.h>
#include <libgxim/gximattr.h>
#include <libgxim/gximclconn.h>
#include <libgxim/gximcltmpl.h>
#include <libgxim/gximconnection.h>
#include <libgxim/gximcore.h>
#include <libgxim/gximmessages.h>
#include <libgxim/gximmisc.h>
#include <libgxim/gximprotocol.h>
#include <libgxim/gximsrvconn.h>
#include <libgxim/gximsrvtmpl.h>
#include <libgxim/gximtransport.h>

#endif /* __LIBGXIM_H__ */
